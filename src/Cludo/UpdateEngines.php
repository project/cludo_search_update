<?php

namespace Drupal\cludo_search_update\Cludo;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Exception\GuzzleException;

class UpdateEngines {

  /**
   * HTTP Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Cludo config settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   HTTP Client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   Path alias manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger factory.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, AliasManagerInterface $alias_manager, LanguageManagerInterface $language_manager, LoggerChannelFactoryInterface $logger_factory) {
    $this->httpClient = $http_client;
    $this->config = $config_factory->get('cludo_search_update.settings');
    $this->aliasManager = $alias_manager;
    $this->languageManager = $language_manager;
    $this->logger = $logger_factory->get('cludo_search_update');
  }

  /**
   * Update the given content in the corresponding Cludo Search Engines.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to update.
   * @param string $action
   *   Action: insert, update, delete.
   */
  public function updateContent($entity, $action) {
    $crawler_languages = [];
    $crawlers = $this->config->get('crawlers');
    $crawlers = explode(',', $crawlers);
    foreach ($crawlers as $crawler) {
      $parts = explode(':', $crawler);
      $crawler_languages[trim($parts[0])] = trim($parts[1]);
    }
    if (empty($crawler_languages)) {
      return;
    }
    // Deletion should only happen if remove_deleted is set.
    if ($action === 'delete' && !$this->config->get('remove_deleted')) {
      return;
    }
    // Remove items from index should only happen if remove_unblished is set.
    if (!$entity->isPublished() && !$this->config->get('remove_unpublished')) {
      return;
    }
    // Add items to index should only happen if add_published is set.
    if ($entity->isPublished() && !$this->config->get('add_published')) {
      return;
    }

    $languages = array_keys($this->languageManager->getLanguages());

    if ($action === 'delete') {
      $this->callFunction($languages, $crawler_languages, $entity, 'deleteItem');
    }
    elseif ($entity->isPublished()) {
      // Post item to index.
      $this->callFunction($languages, $crawler_languages, $entity, 'postItem');
    }
    elseif (!$entity->isPublished() && !empty($entity->original) && $entity->original->isPublished()) {
      // Entity was just unpublished.
      $this->callFunction($languages, $crawler_languages, $entity, 'deleteItem');
    }
    elseif (!$entity->isPublished() && $this->config->get('remove_previously_unpublished')) {
      // Entity was previously unpublished.
      $this->callFunction($languages, $crawler_languages, $entity, 'deleteItem');
    }
  }

  /**
   * Call given function when conditions are met.
   */
  protected function callFunction($languages, $crawler_languages, $entity, $function) {
    $site_domain = $this->config->get('site_domain');
    foreach ($languages as $language) {
      if (!isset($crawler_languages[$language])) {
        continue;
      }
      $alias_current_language = $this->aliasManager->getAliasByPath('/node/' . $entity->id(), $language);
      if ($alias_current_language !== '/node/' . $entity->id()) {
        $id = $site_domain . $this->getLanguagePrefix($language) . $alias_current_language;
        try {
          $this->{$function}($id, $crawler_languages[$language]);
        } catch (GuzzleException $e) {
            $this->logger->error('Failed to POST ' . $function . ' for id ' . $id . ' and language ' . $language. '. Message: ' . $e->getMessage());
        }
      }
    }
  }

  /**
   * Get prefix for given language.
   */
  protected function getLanguagePrefix($language) {
    if ($prefixes = \Drupal::config('language.negotiation')->get('url.prefixes')) {
      if (!empty($prefixes[$language])) {
        return '/' . $prefixes[$language];
      }
    }
    return '';
  }

  /**
   * Delete item from cludo search.
   *
   * @param string $id
   *   Id of the item to delete.
   * @param string $engine
   *   Engine to delete item from.
   */
  protected function deleteItem($id, $crawler_id) {
    $customer_id = $this->config->get('customer_id');
    $domain = trim($this->config->get('domain'), '/');
    $url = $domain . '/api/v3/' . $customer_id . '/content/' . $crawler_id . '/delete';
    $data = [[$id => "PageContent"]];
    $this->httpPost($url, $data);
  }

  /**
   * Post item to cludo search.
   *
   * @param string $id
   *   Id of the item to delete.
   * @param string $engine
   *   Engine to delete item from.
   */
  protected function postItem($id, $crawler_id) {
    $customer_id = $this->config->get('customer_id');
    $domain = trim($this->config->get('domain'), '/');
    $url = $domain . '/api/v3/' . $customer_id . '/content/' . $crawler_id . '/pushurls';
    $data = [$id];
    $this->httpPost($url, $data);
  }

  /**
   * Make an HTTP POST request.
   *
   * @param string $url
   *   URL to post data to.
   * @param array $data
   *   Data to post.
   */
  protected function httpPost($url, $data) {
    $this->httpClient->request('POST', $url, [
      'json' => $data,
      'auth' => [
        $this->config->get('customer_id'),
        $this->config->get('api_key'),
      ],
    ]);
  }

}