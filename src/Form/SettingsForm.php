<?php

namespace Drupal\cludo_search_update\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Cludo Search Update settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cludo_search_update_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cludo_search_update.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cludo API Domain'),
      '#default_value' => $this->config('cludo_search_update.settings')->get('domain'),
      '#required' => TRUE,
    ];

    $form['customer_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cludo API Customer Id'),
      '#default_value' => $this->config('cludo_search_update.settings')->get('customer_id'),
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cludo API API Key'),
      '#default_value' => $this->config('cludo_search_update.settings')->get('api_key'),
      '#required' => TRUE,
    ];

    $form['crawlers'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cludo API Crawlers to update'),
      '#description' => $this->t('Comma separated list of crawlers to update following the format: <em>language:crawler_id</em> (separate them by commas).'),
      '#default_value' => $this->config('cludo_search_update.settings')->get('crawlers'),
      '#required' => TRUE,
    ];

    $form['site_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Domain used to build cludo IDs'),
      '#default_value' => $this->config('cludo_search_update.settings')->get('site_domain'),
      '#required' => TRUE,
    ];

    $form['remove_unpublished'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove items from index when content gets unpublished'),
      '#default_value' => $this->config('cludo_search_update.settings')->get('remove_unpublished'),
    ];

    $form['remove_previously_unpublished'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove items from index when unpublished content is saved no matter if just unpublished or not'),
      '#default_value' => $this->config('cludo_search_update.settings')->get('remove_previously_unpublished'),
    ];

    $form['remove_deleted'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove items from index when content gets deleted'),
      '#default_value' => $this->config('cludo_search_update.settings')->get('remove_deleted'),
    ];

    $form['add_published'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add items to index when content gets published'),
      '#default_value' => $this->config('cludo_search_update.settings')->get('add_published'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $crawlers = $form_state->getValue('crawlers');
    foreach (explode(',', $crawlers) as $crawler) {
      $parts = explode(':', $crawler);
      if (strlen(trim($parts[0])) !== 2) {
        $form_state->setErrorByName('crawlers', $this->t('crawler language code should be 2 characters long.'));
      }
      if (count($parts) !== 2) {
        $form_state->setErrorByName('crawlers', $this->t('Wrong crawlers format.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('cludo_search_update.settings')
      ->set('domain', $form_state->getValue('domain'))
      ->set('customer_id', $form_state->getValue('customer_id'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('crawlers', $form_state->getValue('crawlers'))
      ->set('site_domain', $form_state->getValue('site_domain'))
      ->set('remove_unpublished', $form_state->getValue('remove_unpublished'))
      ->set('remove_previously_unpublished', $form_state->getValue('remove_previously_unpublished'))
      ->set('remove_deleted', $form_state->getValue('remove_deleted'))
      ->set('add_published', $form_state->getValue('add_published'))
    ->save();
    parent::submitForm($form, $form_state);
  }

}
